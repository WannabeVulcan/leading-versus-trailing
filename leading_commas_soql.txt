SELECT
    NEWFIELD
    ,Id
    ,Name
    ,ServiceEnd__c
    ,ProviderId__c
    ,AccountName__c
    ,ServiceType__c
    ,PersonOther__c
    ,DenialReason__c
    ,ServiceStart__c
    ,ProviderName__c
    ,ANOTHERNEWFIELD
    ,ignorePrimacy__c
    ,SAM_Submission__c
    ,ExpenseIncurred__c
    ,Service_Recipient__c
    ,examinerApprovedAmount__c
    ,Case__c
    ,Case__r.Origin
    ,Case__r.CaseNumber
    ,Case__r.ReceivedDate__c
    ,Case__r.Provider_Tax_ID__c
    ,Case__r.SpendingAccount__r.SA_ID__c
    ,Case__r.SpendingAccount__r.SAMKey__c
    ,THEFINALFIELD
FROM CASE
WHERE Id = :my_case_id

